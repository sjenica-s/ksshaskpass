# Translation of ksshaskpass.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: ksshaskpass\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-05-07 03:54+0200\n"
"PO-Revision-Date: 2015-01-17 13:25+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Časlav Ilić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "caslav.ilic@gmx.net"

#: main.cpp:261 main.cpp:339 main.cpp:348 main.cpp:364
#, kde-format
msgid "Ksshaskpass"
msgstr "K‑SSH‑upitnik"

#: main.cpp:263
#, kde-format
msgid "KDE version of ssh-askpass"
msgstr "KDE verzija SSH‑upitnika"

#: main.cpp:265
#, fuzzy, kde-format
#| msgid ""
#| "(c) 2006 Hans van Leeuwen\n"
#| "(c) 2008-2010 Armin Berres"
msgid ""
"(c) 2006 Hans van Leeuwen\n"
"(c) 2008-2010 Armin Berres\n"
"(c) 2013 Pali Rohár"
msgstr ""
"© 2006, Hans van Leuven\n"
"© 2008–2010, Armin Beres"

#: main.cpp:266
#, kde-format
msgid ""
"Ksshaskpass allows you to interactively prompt users for a passphrase for "
"ssh-add"
msgstr ""
"K‑SSH‑upitnik omogućava interaktivno traženje lozinke za ssh-add od korisnika"

#: main.cpp:271 main.cpp:274
#, kde-format
msgid "Armin Berres"
msgstr "Armin Beres"

#: main.cpp:271 main.cpp:274
#, kde-format
msgid "Current author"
msgstr "Trenutni autor"

#: main.cpp:272 main.cpp:275
#, kde-format
msgid "Hans van Leeuwen"
msgstr "Hans van Leuven"

#: main.cpp:272 main.cpp:275
#, kde-format
msgid "Original author"
msgstr "Prvobitni autor"

#: main.cpp:273 main.cpp:276
#, kde-format
msgid "Pali Rohár"
msgstr ""

#: main.cpp:273 main.cpp:276
#, kde-format
msgid "Contributor"
msgstr ""

#: main.cpp:281
#, kde-format
msgctxt "Name of a prompt for a password"
msgid "Prompt"
msgstr "Upit"

#: main.cpp:287
#, kde-format
msgid "Please enter passphrase"
msgstr "Unesite lozinku"
